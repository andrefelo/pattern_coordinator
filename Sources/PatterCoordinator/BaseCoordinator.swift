//
//  BaseCoordinator.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit


open class BaseCoordinator: Coordinator {
  
  
  // ---------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------
  
  
  open var uuid: String
  open var parent: Coordinator!
  open var childCoordinators = [Coordinator]()
  open var navigationController: UINavigationController = .init()
  open var firstCtrl: UIViewController?
  open var callBack: ((Any, String) -> ())?
  
  
  // ---------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------
  
  
  public init(withTabs parent: Coordinator) {
    self.parent = parent
    uuid = NSStringFromClass(type(of: self))
  }
  
  
  public init(navigateWithParent parent: Coordinator) {
    self.parent = parent
    uuid = NSStringFromClass(type(of: self))
    navigationController = parent.root
  }
  
  
  public init(withPresent parent: Coordinator, style: UIModalPresentationStyle = .fullScreen, animated: Bool = true) {
    self.parent = parent
    uuid = NSStringFromClass(type(of: self))
    root.modalPresentationStyle = style
    root.setNavigationBarHidden(true, animated: false)
    self.parent.present(root, animated: animated)
  }
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  open func start(animated: Bool = true ) {
    guard let ctrl = firstCtrl else {
      fatalError("BaseTabBarCoordinator: firstCtrl is null")
    }
    push(ctrl, animated: animated)
  }
  
  
  open func initChildCoordinator(_ coordinator: Coordinator, animated: Bool = true){
    childCoordinators += [coordinator]
    coordinator.start(animated: animated)
  }
  
  
  deinit {
    #if DEBUG
    print("Exit BaseTabBarCoordinator: \(uuid)")
    #endif
  }
}



open class BaseTabBarCoordinator: BaseCoordinator {
  
  
  // ---------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------
  
  
  open var tabBarItem: UITabBarItem? {
    didSet {
      setupBarItem()
    }
  }
  
  open override var firstCtrl: UIViewController? {
    didSet {
      firstCtrl?.title = tabBarItem?.title
    }
  }
  
  
  // ---------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------
  
  
  public override init(navigateWithParent parent: Coordinator) {
    super.init(navigateWithParent: parent)
  }
  
  
  public override init(withTabs parent: Coordinator) {
    super.init(withTabs: parent)
  }
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  private func setupBarItem() {
    guard let item = tabBarItem else { return }
    navigationController.tabBarItem = item
  }
  
  
  open override func start(animated: Bool = true) {
    super.start()
  }
}



open class TabbarCoordinator: BaseCoordinator {
	
	
	public typealias T = UITabBarController
	
	
	// ---------------------------------------------------------------------
	// MARK: Variables
	// ---------------------------------------------------------------------
	
	
	open var tabController: T!
  
}
