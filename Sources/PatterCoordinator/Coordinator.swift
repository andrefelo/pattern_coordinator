//
//  Coordinator.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

public protocol Coordinator {
  
  
  // ---------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------
  
  
  var uuid:String { get set}
  var parent:Coordinator! { get set }
  var childCoordinators: [Coordinator] { get set }
  var navigationController: UINavigationController { get set }
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  /// funcion para inicializar el coordinador
  /// - Parameter animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  func start(animated: Bool) -> Void
}



extension Coordinator {
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  /// navigation controller del coordinator
  public var root:UINavigationController {
    return navigationController
  }
  
  
  /// Push ViewController
  /// - Parameters:
  ///   - viewController: The view controller to push onto the stack. This object cannot be a tab bar controller. If the view controller is already on the navigation stack, this method throws an exception.
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  public func push(_ viewController: UIViewController, animated: Bool = true) {
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  
  /// Present ViewController
  /// - Parameters:
  ///   - viewController: controlador que llega como parametro
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  ///   - completion
  public func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
    navigationController.present(viewController, animated: animated, completion: completion)
  }
  
  
  /// Close the ViewController doing a pop
  /// - Parameter animated: define si se quiere mostrar la animación
  public func pop(animated: Bool = true) {
    navigationController.popViewController(animated: animated)
  }
  
  
  /// Close the ViewController doing a popToRoot
  /// - Parameter animated: Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  public func popToRoot(animated: Bool = true) {
    navigationController.popToRootViewController(animated: animated)
  }
  
  
  /// Close the ViewController doing a dismiss
  /// - Parameters:
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  ///   - completion: se requiere hacer un proceso previo antes de finalizar la desvinculacion
  public func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
    navigationController.dismiss(animated: animated, completion: completion)
  }
  
  
  /// Close the ViewController, this function checks what kind of presentation has the controller and then it make a dismiss or pop
  /// - Parameters:
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  ///   - completion: se requiere hacer un proceso previo antes de finalizar la desvinculacion
  public func close(isController: Bool = false, animated: Bool = true, completion: (() -> Void)? = nil) {
    let ask = !isController ? root.isModal : root.viewControllers.last?.isModal == true
    if ask {
      dismiss(animated: animated) {
        completion?()
      }
    } else {
      pop(animated: animated)
      completion?()
    }
  }
  
  
  /// Remove its child coordinators
  /// - Parameters:
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  ///   - coordinator: Coordinator
  ///   - completion
  mutating public func removeChild(animated: Bool = true, coordinator : Coordinator, completion:(() -> Void)? = nil) {
    if let index = childCoordinators.firstIndex(where: {$0.uuid == coordinator.uuid}) {
      coordinator.removeChids(animated: animated)
      childCoordinators.remove(at: index )
    }
    completion?()
  }
  
  
  /// Remove its child coordinators
  /// - Parameter animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  public func removeChids(animated: Bool = true, completion:(() -> Void)? = nil){
    for coordinator in childCoordinators {
      coordinator.close(animated: animated) {
        coordinator.clearCoordinator()
        coordinator.navigationController.viewControllers = []
      }
    }
    completion?()
  }
  
  
  /// Close the current navigation controller and then removes it from its coordinator parent
  /// - Parameters:
  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
  ///   - completion
  public func finish(animated: Bool = true, completion: (() -> Void)?) {
    var aux = self
    
    if aux.parent != nil {
      aux.parent?.removeChild(
        animated: animated,
        coordinator: self,
        completion: {
          self.close(animated: animated) {
            self.clearCoordinator()
            completion?()
          }
        }
      )
    } else {
      aux.removeChids(animated: animated, completion: completion)
      aux.childCoordinators.removeAll()
    }
  }
  
  // Clear its properties
  func clearCoordinator() {
    if let item = self as? TabbarCoordinator {
      item.tabController?.viewControllers = nil
      item.tabController = nil
      item.callBack = nil
      item.firstCtrl = nil
      item.root.viewControllers = []
    } else if let item = self as? BaseCoordinator{
      item.firstCtrl = nil
      item.callBack = nil
      item.root.viewControllers = []
    }
  }
  
  
  /// Get the top coordinator
  /// - Parameters:
  ///   - appCoordinator: Main coordinator
  ///   - pCoodinator:
  public func topCoordinator(pCoodinator: Coordinator? = nil) -> Coordinator? {
    
    guard childCoordinators.last != nil else {
      return self
    }
    
    var auxCoordinator = pCoodinator ?? self.childCoordinators.last
    
    guard let tabCoordinator = auxCoordinator as? TabbarCoordinator
    else {
      return getDeepCoordinator(
        from: &auxCoordinator
      )
    }
    
    let itemSelected        = tabCoordinator.tabController.selectedIndex
    let coordinatorSelected = tabCoordinator.childCoordinators[itemSelected]
    auxCoordinator          = coordinatorSelected.childCoordinators.last
    
    if let coord = auxCoordinator as? TabbarCoordinator {
      return self.topCoordinator(pCoodinator: coord)
    } else {
      auxCoordinator = coordinatorSelected
      return getDeepCoordinator(
        from: &auxCoordinator
      )
    }
  }
  
  
  /// Get the deepest coordinator from a given coordinator as parameter
  /// - Parameters:
  ///   - value: Coordinator
  private func getDeepCoordinator(from value:inout Coordinator?) -> Coordinator?{
    var rhs: Coordinator?
    while rhs == nil {
      if value?.childCoordinators.last == nil {
        rhs = value
      } else {
        value = value?.childCoordinators.last
      }
    }
    return rhs
  }
  
  
  // Restart coordinator
  public func restart(animated: Bool, completion: (() -> Void)?) {
    while true {
      guard let coordinator = topCoordinator() else { return }
      if coordinator.parent == nil {
        completion?()
        return
      } else {
        coordinator.finish(animated: false, completion: nil)
      }
    }
  }
}



public extension UIViewController {
  var isModal: Bool {
    if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
      return false
    } else if presentingViewController != nil {
      return true
    } else if let navigationController = navigationController, navigationController.presentingViewController?.presentedViewController == navigationController {
      return true
    } else if let tabBarController = tabBarController, tabBarController.presentingViewController is UITabBarController {
      return true
    } else {
      return false
    }
  }
}
